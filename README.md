# Unbound 2.0

This runs a Unbound instance - I use it for my LAN DNS so I consider it as production ready.

If you have more generic questions please have a look [here](https://cms.manhart.space/dockerisierung)

## Getting Started

Copy the `.env-example` to `.env`, now you can start the container.

For the service to do something useful create some unbound configuration files in `./data/`. A good example is to resolve some servernames / domain names to their internal IP Address or if you have a server only available in the intranet, etc.

If you have created some rules, you can set the `HEALTHCHECK_HOSTNAME` in your `.env` file so that the healthcheck will fail if there is something not working correctly with unbound but the process is still alive.

### Manual steps

None

### Build / Extend

Since there is no actively maintained unbound container, I switched to building my own. But for now it is very simple without tagging etc.

### Replicating the service

Not available, due to the neccessary port mappings. Also not needed since this is a very lightweight / stable service.

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down
	
to tear down the service.

Then update your `gliderlabs/alpine` image with

	# docker pull gliderlabs/alpine

Lastly start the container with

	# docker-compose up -d --build

so the image will be rebuilt.

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/unbound/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author.

### Changelog

__2.0__

* Switched from an not anymore maintained image to building our own.
* Created README

### Open issues

* The container uses ROOT as user - we should change that in our `dockerfile`

## Sources

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [Alpine Image](https://hub.docker.com/r/yobasystems/alpine-mariadb)
* [Adding a Healthcheck](https://howchoo.com/g/zwjhogrkywe/how-to-add-a-health-check-to-your-docker-container)
